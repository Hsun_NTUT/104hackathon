import React, { Component } from 'react';
import { getLocationName } from './utils';
import { SVGMap, Taiwan } from 'react-svg-map';
import 'react-svg-map/lib/index.css';
import CardList from './cardList';
import Progress from 'circular-progress-angular-gradient';
import Tip from '../tip/Tip';
import Pagination from '../pagination/Pagination';
import BackgrundImage from '../../../images/bg-grid.png';

let context;

class map extends Component {
	constructor(props) {
		super(props);
		context = this;
		this.state = {
			page: 1,
			pointedLocation: 'Taipei City',
			tooltipStyle: {
				display: 'none'
			}
		};
		this.handleLocationMouseOver = this.handleLocationMouseOver.bind(this);
		this.handleLocationMouseOut = this.handleLocationMouseOut.bind(this);
		this.handleLocationMouseMove = this.handleLocationMouseMove.bind(this);
		this.handleLocationClick = this.handleLocationClick.bind(this);
	}

	componentDidMount() {
		// this.setState({pointedLocation:'New Taipei City'});
		// document.querySelector('path[id="taipei-city"]').click();
	}

	componentWillReceiveProps(nextProps) {
		const { count } = this.props;
		if (count != nextProps.count) {
			this.refs.mapBox &&
				this.refs.mapBox.scrollIntoView({ block: 'start', behavior: 'smooth' });
		}
	}

	handleLocationMouseOver(event) {
		const pointedLocation = getLocationName(event);
		this.setState({ pointedLocation });
	}

	handleLocationMouseOut() {
		// this.setState({ pointedLocation: null, tooltipStyle: { visibility: 'hidden' } });
	}

	handleLocationMouseMove(event) {
		const tooltipStyle = {
			visibility: 'visible',
			display: 'block',
			top: event.clientY + 10,
			left: event.clientX - 100
		};
		this.setState({ tooltipStyle });
	}

	handleLocationClick(event) {
		const pointedLocation = getLocationName(event),
			countryBody = event.target.getBoundingClientRect(),
			tooltipBody = this.refs.tooltip.getBoundingClientRect();
		// console.log(window.scrollY);
		// console.log(this.refs.tooltip);
		const tooltipStyle = {
			display: 'flex',
			position: 'absolute',
			top: (countryBody.top + countryBody.bottom) / 2 - (tooltipBody.bottom - tooltipBody.top) + window.scrollY - 50,
			left: (countryBody.left + countryBody.right) / 2 - (tooltipBody.right - tooltipBody.left) / 2
		};
		const { onLocationClick } = this.props;
		onLocationClick && onLocationClick(pointedLocation);
		context.setState({ pointedLocation, tooltipStyle });
		// // const tooltipStyle = {
		// // 	display: 'none',
		// // 	top: event.clientY + 10,
		// // 	left: event.clientX - 100
		// // };
		// // this.setState({ tooltipStyle });
	}

	render() {
		const { onClick, mapData, keyword } = this.props, { pointedLocation } = this.state;
		console.log('mapData', mapData);
		return (
			<div className='map' ref='mapBox'>
				<div className='map__box' >
					<img className='map__box__backgorund' src={BackgrundImage} />
					<div className='map__box_progress-left'>
						{mapData && mapData.country && mapData.country.goodScore ?
							<Tip
								width={90}
								align='center'
								content={<span>三節獎金<br />年終獎金<br />生日禮金<br />結婚津貼<br />生育津貼<br />專利獎金<br />久任獎金<br />團保<br />股票<br />健康檢查<br />分紅獎金<br />全勤獎金<br />分紅入股<br />績效獎金<br />旅遊津貼<br />體檢<br />旅遊補助<br />慰問金</span>}
								type='left' >
								<div className='map__box_progress-left-score'>{parseInt(mapData.country.goodScore * 3)}</div>
								<div className='map__box_progress-left-line' />
								<Progress
									colorEmpty='#FFFFFF00'
									// colorFill='#FFFFFF'
									colorEnd='#d0e857'
									colorStart='#8cc63f'
									percentage={(mapData.country.goodScore / 100 * 3 > 100) ? 100 : (mapData.country.goodScore / 100 * 3)}
									strokeLinecap='round'
									strokeWidth={3}
									width={150} />
							</Tip> : null}
					</div>
					<div className="examples__block__map">
						<div className="map__box_map-title">{pointedLocation + '  的"' + keyword + '"平均福利指數'}</div>
						<SVGMap
							map={Taiwan}
							onLocationClick={this.handleLocationClick}
							// onLocationMouseOver={this.handleLocationMouseOver}
							onLocationMouseOut={this.handleLocationMouseOut}
						// onLocationMouseMove={this.handleLocationMouseMove}
						/>
					</div>
					<div className='map__box_progress-right'>
						{mapData && mapData.country && mapData.country.badScore ?
							<Tip
								width={90}
								align='center'
								content={<span>加班<br />輪班<br />面洽<br />面談<br />常駐</span>}
								type='right' >
								<div className='map__box_progress-right-score'>{parseInt(-mapData.country.badScore * 3)}</div>
								<div className='map__box_progress-right-line' />
								<Progress
									colorEmpty='#FFFFFF00'
									// colorFill='#FFFFFF'
									colorEnd='#a18cd1'
									colorStart='#fbc2eb'
									percentage={(-mapData.country.badScore / 100 * 3) > 100 ? 100 : (-mapData.country.badScore / 100 * 3)}
									strokeLinecap='round'
									strokeWidth={3}
									width={150} />
							</Tip> : null}
					</div>

					<div className="examples__block__map__tooltip"
						style={this.state.tooltipStyle} ref='tooltip'>
						<div className="examples__block__map__tooltip-box">
							{pointedLocation}
						</div>
						<div className="examples__block__map__tooltip-arrow" />
					</div>
				</div>
				<div className='map__cardlist'>
					{mapData.jobList && mapData.jobList.length > 0 ?
						< CardList data={mapData.jobList} onClick={(type) => onClick && onClick(type)} /> :
						<div className="map__box_map-title">此城市沒有相關職缺</div>}
				</div>

				<Pagination
					amount={21}
					range={10}
					current={this.state.page}
					onChange={(page) => {
						const { onPageCange } = this.props;
						onPageCange && onPageCange(page);
						this.setState({ page });
					}} />
			</div>
		);
	}
}

map.defaultProps = {
	keyword: '工程師',
	mapData: {
		country: {

		},
		jobList: null
	}
};

export default map;