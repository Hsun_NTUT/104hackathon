import React, { Component } from 'react';
import './card.scss';

import LikeImage from '../../../images/like.png';
import LikeApply from '../../../images/apply.png';

class card extends Component {

	constructor(props) {
		super(props);
	}

	handleClick(type) {
		const { onClick } = this.props;
		onClick && onClick(type);
	}


	render() {
		const { jobName, jobCompany, goodScore, badScore } = this.props.data;
		return (
			<div className='card'>
				<div className='card-item'>
					<div className='job-name'>{jobName}</div>
					<div className='comapny-name'>{jobCompany}</div>
					<div className='score-wrap'>
						<div className='score-good card-score'>
							<div className='circle'>
								<div className='card-value'>{goodScore * 3}</div>
							</div>
							<div className='score-name'>Good job Score</div>
						</div>
						<div className='score-bad card-score'>
							<div className='circle'>
								<div className='card-value'>{-badScore * 3}</div>
							</div>
							<div className='score-name'>Good job Score</div>
						</div>
					</div>
					<div className='card-action-wrap'>
						<div className='card-like-wrap' onClick={() => this.handleClick('like')}>
							<div className='like-img'><img src={LikeImage} alt="" />Like</div>
						</div>
						<div className='card-apply-wrap' onClick={() => this.handleClick('apply')}>
							<div className='apply-img'><img src={LikeApply} alt="" />Apply</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

card.defaultProps = {
	data: {
		jobName: '沒資料',
		jobCompany: '沒資料',
		goodScore: 100,
		badScore: 100,
	}
};

export default card;