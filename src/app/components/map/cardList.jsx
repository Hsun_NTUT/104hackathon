import React, { Component } from 'react';
import Card from './card';
import './cardlist.scss';

class cardList extends Component {

	handleClick(type) {
		const { onClick } = this.props;
		onClick && onClick(type);
	}

	render() {
		const { data } = this.props;
		return (
			<div className='cardList'>
				{data.map((item, index) =>
					<Card key={index} data={item}
						onClick={(type) => this.handleClick(type)} />)}
			</div>
		);
	}
}

cardList.defaultProps = {
	data: []
};

export default cardList;