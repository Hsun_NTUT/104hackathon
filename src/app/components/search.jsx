import React, { Component } from 'react';
import classnames from 'classnames';
import './search.scss';
import MdKeyboardArrowRight from '../../images/arrow-right.png';

class search extends Component {

	constructor(props) {
		super(props);
		this.state = {
			keyword: '工程師',
			keyword_error: false
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleSearch = this.handleSearch.bind(this);
	}

	handleChange(e) {
		this.setState({ keyword: e.target.value });
	}

	handleEnter(e) {
		if (e.key === 'Enter') {
			this.handleSearch();
		}
	}

	handleSearch() {
		const { onSearch } = this.props, { keyword } = this.state;
		if (keyword.length == 0) {
			this.setState({ keyword_error: true });
		} else {
			onSearch && onSearch(keyword);
			this.setState({ keyword_error: false });
		}
	}

	render() {
		const { keyword, keyword_error } = this.state;
		return (
			<div className='search'>
				<div className='search-title'>
					Welcome to <br />APPLING JOB WEIGHT CALCULATE
				</div>
				<div className='search-subtitle'>
					職缺福利計算機
				</div>
				<div className={classnames('search-searchbar',
					{ 'search-searchbar--error': keyword_error })}>
					<input
						defaultValue={keyword}
						onChange={this.handleChange}
						className='search-searchbar__input'
						onKeyPress={this.handleEnter.bind(this)}
						placeholder='Search Job' />
					<div className='search-searchbar__bt'
						onClick={this.handleSearch}>
						<img src={MdKeyboardArrowRight} className='search-searchbar__bt-icon' />
					</div>
				</div>
				<div className="search-searchbar__hint">--------您可以搜尋--------<br />行政、助理、無經驗可、英文、作業員、業務、<br />
				司機、咖啡、工程師、倉管、遊戲、急徵 立即上班、java...</div>
			</div>
		);
	}
}

export default search;