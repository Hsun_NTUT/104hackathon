import React, { Component } from 'react';
import './result.scss';
import Image from '../../images/result.png';

class result extends Component {


	componentWillReceiveProps(nextProps) {
		const { count } = this.props;
		if (count != nextProps.count) {
			this.resultBox.scrollIntoView({ behavior: 'smooth' });
		}
	}

	render() {
		return (
			<div className='result' ref={(el) => { this.resultBox = el; }} >
				<img className='result-image' src={Image} />
			</div>
		);
	}
}

export default result;