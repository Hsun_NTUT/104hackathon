'use strict';

/**
 * Component : pagination
 * @param { number } range - 一次秀幾頁
 * @param { number } current - 目前頁數
 * @param { number } amount - 總頁數
 * @param { function } onChange - on page change event callback
 */
import './pagination.scss';
import React, { Component } from 'react';
import classNames from 'classnames';

import MdKeyboardArrowRight from './arrow-right.png';
import MdKeyboardArrowLeft from './arrow-left.png';
// import MdKeyboardArrowRight from 'react-icons/lib/md/chevron-right';
// import MdKeyboardArrowLeft from 'react-icons/lib/md/chevron-left';


class Pagination extends Component {
	constructor(props) {
		super(props);
		this.state = {
			current: Number(props.current)
		};
	}

	componentWillReceiveProps(nextProps) {
		const { current } = nextProps;
		this.setState({ current: Number(current) });
	}

	setPage(n) {
		if (n === this.state.current) return;

		const { amount } = this.props;
		//Min
		n = n < 1 ? 1 : n;
		//Max
		n = n > amount ? amount : n;
		this.setState({ current: n });

		this.props.onChange && this.props.onChange(n);
	}

	getRange(current) {
		const { range, amount } = this.props,
			start = current - Math.floor(range / 2),
			end = current + Math.ceil(range / 2) - 1;
		//Amount Less Range
		if (range >= amount) return this.buildRangeList(1, amount);
		//Current In Front
		if (start < 1 && end - start + 1 <= amount) return this.buildRangeList(1, end - start + 1);
		//Current In After
		if (end > amount && start - (end - amount) >= 1) return this.buildRangeList(start - (end - amount), amount);
		//Current In Middle
		return this.buildRangeList(start, end);
	}

	buildRangeList(start, end) {
		const list = [];
		for (let i = start; i <= end; i++) {
			list.push(i);
		}
		return list;
	}

	render() {
		const self = this,
			{ current } = this.state,
			{ amount } = this.props,
			isFirst = current === 1,
			isLast = current === amount,
			pages = this.getRange(current),
			hasPages = pages.length > 0,
			pagesList = pages.map(function (n) {
				return <button key={n} name={n}
					className={classNames('pagination__btn', { 'pagination__btn--active': current === n })}
					onClick={() => self.setPage(n)} > {n} </button>;
			});

		return (
			<div className="pagination">
				<span className="pagination__btn-group">
					<button
						className={classNames('pagination__btn', 'pagination__btn--prev', { 'pagination__btn--disabled': !hasPages || isFirst })}
						onClick={() => hasPages && !isFirst && this.setPage(this.state.current - 1)}>
						<img src={MdKeyboardArrowLeft} style={{ width: 8, height: 8 }} />
					</button>
					{pagesList}
					<button
						className={classNames('pagination__btn', 'pagination__btn--next', { 'pagination__btn--disabled': !hasPages || isLast })}
						onClick={() => hasPages && !isLast && this.setPage(this.state.current + 1)}>
						<img src={MdKeyboardArrowRight} style={{ width: 8, height: 8 }} />
					</button>
				</span>
				{/* <span className="pagination__info">Page {current} of {amount}</span> */}
			</div>
		);
	}
}

Pagination.defaultProps = {
	range: 10,
	current: 6,
	amount: 10,
	onChange: null
};

export default Pagination;