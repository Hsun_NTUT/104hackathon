'use strict';
/**
 * Component : Tip
 * @param { string } className - css樣式
 * @param { string } content - 顯示內容 string || element
 * @param { string } type - 顯示位置 top || right || bottom || left
 * @param { number } width - 限制寬度會讓文字折行
 * @param { string } align - 文字對齊 left || center || right
 * @param { boolean } disabled - 隱藏tooltip
 * @param { boolean } enabledHover - 可以懸停在tooltip上
 */
import React, { Component } from 'react';
import classNames from 'classnames';
import { CSSTransitionGroup } from 'react-transition-group';
import TipContent from './TipContent';
import './tip.scss';

export default class Tip extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isHover: false
		};
		this.delayTimeout = null;
	}

	handleEnter = () => {
		const { disabled } = this.props;
		if (disabled) return;
		this.delayTimeout = setTimeout(() => {
			this.setState({ isHover: true });
		}, this.props.delay);
	}

	handleLeave = () => {
		clearTimeout(this.delayTimeout);
		this.setState({ isHover: false });
	}

	render() {
		let { className, content, type, align, width, enabledHover } = this.props;
		const dir =
			{
				left: 'tip--left',
				right: 'tip--right',
				top: 'tip--top',
				bottom: 'tip--bottom'
			}, textAlign = {
				left: 'tip__content--left',
				center: 'tip__content--center',
				right: 'tip__content--right'
			}, style = width && {
				width,
				whiteSpace: 'normal'
			}, { isHover } = this.state;
		return (
			<div className="tooltip" onMouseEnter={this.handleEnter} onMouseLeave={this.handleLeave} ref={ref => this.container = ref}>
				{this.props.children}
				<CSSTransitionGroup
					transitionName="tip--slide"
					transitionEnterTimeout={200}
					transitionLeaveTimeout={100}>
					{isHover && (
						<TipContent parent={this.container} direction={dir[type]} type={type}>
							<div className={classNames('tip__container', { 'tip__container--enabled-hover': enabledHover }, className)}>
								<div className="tip__arrow"></div>
								<div className={classNames('tip__content', textAlign[align])} style={style}>
									{content}
								</div>
							</div>
						</TipContent>
					)}
				</CSSTransitionGroup>
			</div>
		);
	}
}

Tip.defaultProps = {
	type: 'top',
	width: null,
	align: 'left',
	enabledHover: false,
	disabled: false,
	delay: 100
};
