import React, { Component } from 'react';
import ReactDOM from 'react-dom';

const createElement = (className) => {
	let el = document.querySelector('.' + className);
	if (!el) {
		const body = document.querySelector('body');
		el = document.createElement('div');
		el.setAttribute('class', className);
		body.appendChild(el);
	}
	return el;
};

const getPos = (parent, direction) => {
	const box = parent.getBoundingClientRect();
	switch (direction) {
		case 'top':
			return { top: box.top, left: box.left + (box.width / 2) };
		case 'right':
			return { top: box.top + (box.height / 2), left: box.right };
		case 'bottom':
			return { top: box.bottom, left: box.left + (box.width / 2) };
		case 'left':
			return { top: box.top + (box.height / 2), left: box.left };
		default:
			return { top: box.top, left: box.left + (box.width / 2) };
	}
};
let tipRoot = createElement('tips');

class TipContent extends Component {
	constructor(props) {
		super(props);
		this.el = document.createElement('div');
		this.el.setAttribute('class', `tip ${props.direction}`);
	}

	componentDidMount() {
		this.tip = tipRoot.appendChild(this.el);
		if (!this.props.parent) return;
		const { parent, type } = this.props,
			pos = getPos(parent, type);
		this.tip.style.left = pos.left + 'px';
		this.tip.style.top = pos.top + 'px';
	}

	componentWillUnmount() {
		tipRoot.removeChild(this.el);
	}

	render() {
		return ReactDOM.createPortal(
			this.props.children,
			this.el,
		);
	}
}


export default TipContent;