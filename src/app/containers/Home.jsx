'use strict';

import React, { Component } from 'react';
// import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import axios from 'axios';
// import classNames from 'classnames';

import Search from '../components/search';
import Map from '../components/map/map';
import Result from '../components/result';
import BackgroundImage from '../../images/background.png';
import Loadable from 'react-loading-overlay';

class Home extends Component {
	constructor(props) {
		super(props);
		this.page = 1;
		this.locationId = 6001001;
		this.state = {
			count: 1,
			count2: 1,
			mapData: {},
			loadMaskActive: false
		};
		this.handleClick = this.handleClick.bind(this);
		this.handleSearch = this.handleSearch.bind(this);
		this.handleLocationClick = this.handleLocationClick.bind(this);
		this.handlePageChange = this.handlePageChange.bind(this);
	}

	handleClick(type) {
		this.setState({ count: this.state.count + 1 });
		switch (type) {
			case 'apply':
				console.log(type);
				break;
			case 'like':
				console.log(type);
				break;
		}
	}

	handleSearch(keyword) {
		this.keyword = keyword;
		this.setState({ count2: this.state.count2 + 1, keyword });
		this.searchGO();
	}

	handleLocationClick(locationName) {
		const location = {
			'Keelung City': 6001004,
			'Taipei City': 6001001,
			'New Taipei City': 6001002,
			'Taoyuan City': 6001005,
			'Hsinchu County': 6001006,
			'Hsinchu City': 6001006,
			'Miaoli County': 6001007,
			'Yilan County': 6001003,
			'Yunlin County': 6001012,
			'Taichung City': 6001008,
			'Hualien County': 6001020,
			'Nantou County': 6001011,
			'Changhua County': 6001010,
			'Chiayi County': 6001013,
			'Chiayi City': 6001013,
			'Tainan City': 6001014,
			'Kaohsiung City': 6001016,
			'Taitung County': 6001019,
			'Pingtung County': 6001018
		};
		this.locationId = location[locationName];
		this.searchGO();
	}

	handlePageChange(page) {
		this.page = page;
		this.searchGO();
	}

	searchGO() {
		console.log('page', this.page);
		console.log('keyword', this.keyword);
		console.log('locationId', this.locationId);
		this.setState({ loadMaskActive: true });
		axios.get('http://220.135.22.69:8080/104hackathon/score/area?key=' +
			this.keyword + '&countryId=' + this.locationId + '&size=6&page=' + this.page)
			// axios.get('data/api.json')
			.then(response => {
				//success
				// console.log(response.data);
				this.setState({ mapData: response.data });
			}).catch(error => {
				//failed
				console.log(error);
			}).then(() =>
				this.setState({ loadMaskActive: false })
			);
	}


	render() {
		const { count, count2, mapData, loadMaskActive, keyword } = this.state;

		return (
			<div className="home">
				<img className='home__background' src={BackgroundImage} />
				<Search onSearch={this.handleSearch} />
				<Map
					keyword={keyword}
					mapData={mapData}
					count={count2}
					onClick={this.handleClick}
					onLocationClick={this.handleLocationClick}
					onPageCange={this.handlePageChange} />
				<Result count={count} />
				<div style={{ position: 'fixed', top: 0, height: '100%', width: '100%', display: loadMaskActive == true ? 'unset' : 'none' }}>
					<Loadable
						active={loadMaskActive}
						spinner
						animate
						background='#33333388'
						zIndex={10}
						text={<div style={{ fontSize: 30, fontWeight: 'bold' }}>資料載入中 請稍後</div>}>
					</Loadable>
				</div>
			</div>
		);
	}
}

export default connect()(Home);